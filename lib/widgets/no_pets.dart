import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class NoPetsColumn extends StatelessWidget {
  const NoPetsColumn({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FaIcon(
          FontAwesomeIcons.dog,
          color: Colors.grey,
        ),
        SizedBox(
          height: 10,
        ),
        Text("No pets are added"),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pet_walk/widgets/button.dart';
import 'package:pet_walk/widgets/text_heading.dart';

class UserProfileEditScreen extends StatelessWidget {
  static const routeName = "/user-profile-edit";

  String name, location, phone;

  var _profileFormKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: TextHeading(text: "Profile"),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Edit your profile"),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Form(
                  key: _profileFormKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            TextFormField(
                              initialValue: "John Doe",
                              decoration:
                                  InputDecoration(labelText: "Full Name"),
                              onSaved: (newValue) {
                                name = newValue;
                              },
                              validator: (value) =>
                                  value.isEmpty ? "Name cannot be empty" : null,
                            ),
                            TextFormField(
                              initialValue: "9722129983",
                              decoration: InputDecoration(labelText: "Phone"),
                              onSaved: (newValue) {
                                phone = newValue;
                              },
                              validator: (value) => value.isEmpty
                                  ? "Phone number cannot be empty"
                                  : null,
                              keyboardType: TextInputType.phone,
                            ),
                            TextFormField(
                              initialValue: "2122 Street Name, Dallas, TX",
                              decoration:
                                  InputDecoration(labelText: "Full Address"),
                              onSaved: (newValue) {
                                location = newValue;
                              },
                              validator: (value) => value.isEmpty
                                  ? "Address cannot be empty"
                                  : null,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 30),
                      Container(
                        padding: const EdgeInsets.all(8.0),
                        width: double.infinity,
                        child: Button(
                          label: "Save Profile",
                          onPressed: () {
                            if (_profileFormKey.currentState.validate()) {
                              _profileFormKey.currentState.save();
                              print(name);
                              Navigator.pop(context);
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

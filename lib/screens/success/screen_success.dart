import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pet_walk/widgets/button.dart';
import 'package:pet_walk/widgets/text_heading.dart';

class SuccessScreen extends StatelessWidget {
  static const routeName = '/success';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Container(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextHeading(text: "Success"),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Your Request has been recorded"),
              ),
              Center(
                child: Container(
                  width: 400,
                  height: 350,
                  child: SvgPicture.asset("assets/artworks/pet-in-house.svg"),
                ),
              ),
              Spacer(),
              Container(
                width: double.infinity,
                child: Button(
                  label: "Go Back",
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              SizedBox(height: 40),
            ],
          ),
        ),
      ),
    );
  }
}

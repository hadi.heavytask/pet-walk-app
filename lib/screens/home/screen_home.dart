import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pet_walk/config/constants.dart';
import 'package:pet_walk/screens/user_profile/screen_user_profile.dart';
import 'package:pet_walk/widgets/text_heading.dart';

import 'components/home_body.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text(
                "Pet Walk App",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 22,
                ),
              ),
              decoration: BoxDecoration(
                color: kPrimaryColor,
              ),
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text('Profile'),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
            ListTile(
              leading: Icon(Icons.pending_actions),
              title: Text('Pending Requests'),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text('Log out'),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FaIcon(FontAwesomeIcons.paw),
            SizedBox(
              width: 8,
            ),
            Text(
              "PetWalk",
              style: TextStyle(
                color: kPrimaryDarkColor,
              ),
            )
          ],
        ),
        actions: [
          IconButton(
              icon: FaIcon(
                FontAwesomeIcons.solidUser,
              ),
              onPressed: () {
                Navigator.pushNamed(context, UserProfileScreen.routeName);
              })
        ],
      ),
      body: HomeScreenBody(),
    );
  }
}

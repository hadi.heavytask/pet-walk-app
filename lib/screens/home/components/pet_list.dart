import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pet_walk/config/constants.dart';
import 'package:pet_walk/cubit/pet_list_cubit.dart';
import 'package:pet_walk/screens/pet_add/screen_pet_add.dart';
import 'package:pet_walk/widgets/no_pets.dart';

import 'title_with_arrow.dart';

class PetListContainer extends StatelessWidget {
  const PetListContainer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: kContainerSpacing,
      child: Column(
        children: [
          TitleWithArrow(
            title: "Your pets",
            onPressed: () {
              Navigator.pushNamed(context, PetAddScreen.routeName);
            },
            trailingIcon: Icon(Icons.add),
          ),
          BlocBuilder<PetListCubit, PetListState>(
            builder: (context, state) {
              return state.petList.length == 0
                  ? NoPetsColumn()
                  : Column(
                      children: state.petList
                          .map(
                            (pet) => Card(
                              child: ListTile(
                                onTap: () {},
                                leading: FaIcon(
                                  FontAwesomeIcons.paw,
                                  color: kPrimaryDarkColor,
                                ),
                                title: Text(pet.name),
                              ),
                            ),
                          )
                          .toList(),
                    );
            },
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:pet_walk/config/constants.dart';
import 'package:pet_walk/screens/become_caregiver/screen_become_caregiver.dart';
import 'package:pet_walk/screens/dog-walker-registration/screen-dog-walker-registration.dart';

class BannerContainer extends StatelessWidget {
  const BannerContainer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: kPrimaryExtraLightColor,
        borderRadius: BorderRadius.circular(10),
      ),
      margin: kContainerSpacing,
      width: double.infinity,
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Want to become caregiver?",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            "Play with pets and earn",
            style: TextStyle(
              color: kPrimaryTextColor,
            ),
          ),
          SizedBox(
            height: 4,
          ),
          FlatButton(
            onPressed: () {
              Navigator.pushNamed(
                  context, DogWalkerRegistrationScreen.routeName);
            },
            child: Text(
              "Become Caregiver",
              style: TextStyle(
                color: kPrimaryDarkColor,
                fontWeight: FontWeight.bold,
              ),
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
            color: Colors.white,
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:pet_walk/config/constants.dart';
import 'package:pet_walk/screens/home/components/title_with_arrow.dart';
import 'package:pet_walk/screens/service/screen_service.dart';

import 'feature_card.dart';

class FeatureContainer extends StatelessWidget {
  const FeatureContainer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _walkServices = [
      {
        "name": "Dog Walk 1",
        "desc": "20 minutes walk",
        "price": "15",
      },
      {
        "name": "Dog Walk 2",
        "desc": "30 minutes walk",
        "price": "20",
      },
      {
        "name": "Dog Walk 3",
        "desc": "60 minutes walk",
        "price": "30",
      }
    ];
    var _petBoarding = [
      {
        "name": "Pet Boarding 1",
        "desc": "20 minutes",
        "price": "15",
      },
      {
        "name": "Pet Boarding 2",
        "desc": "30 minutes",
        "price": "20",
      },
      {
        "name": "Pet Boarding 3",
        "desc": "60 minutes",
        "price": "30",
      }
    ];
    var _inHomeTraining = [
      {
        "name": "In Home Training 1",
        "desc": "30 minutes",
        "price": "20",
      },
      {
        "name": "In Home Training 2",
        "desc": "60 minutes",
        "price": "30",
      }
    ];
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TitleWithArrow(
            title: "What are you looking for?",
            onPressed: () {},
            trailingIcon: Icon(Icons.arrow_forward_ios),
          ),
          SizedBox(
            height: 4,
          ),
          Container(
            height: 230,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                FeatureCard(
                  artwork: "dog-walking",
                  title: "Dog Walking",
                  onTap: () => {
                    Navigator.of(context).pushNamed(ServiceScreen.routeName,
                        arguments: _walkServices)
                  },
                ),
                FeatureCard(
                  artwork: "good-dog",
                  title: "Boarding",
                  onTap: () {
                    Navigator.of(context).pushNamed(ServiceScreen.routeName,
                        arguments: _petBoarding);
                  },
                ),
                FeatureCard(
                  artwork: "house",
                  title: "In Home Training",
                  onTap: () {
                    Navigator.of(context).pushNamed(ServiceScreen.routeName,
                        arguments: _inHomeTraining);
                  },
                ),
                // FeatureCard(
                //   artwork: "digital-training",
                //   title: "Digital Training",
                //   onTap: () {
                //     Navigator.of(context).pushNamed(ServiceScreen.routeName,
                //         arguments: _inHomeTraining);
                //   },
                // ),
                // FeatureCard(
                //   artwork: "medical-care",
                //   title: "24/7 Virtual Veterinary Care",
                //   onTap: () {
                //     Navigator.of(context).pushNamed(ServiceScreen.routeName,
                //         arguments: _inHomeTraining);
                //   },
                // ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

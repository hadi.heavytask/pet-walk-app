import 'package:flutter/material.dart';
import 'package:pet_walk/config/constants.dart';

class IntroTextContainer extends StatelessWidget {
  const IntroTextContainer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: kContainerSpacing,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                "Hi",
                style: TextStyle(
                  fontSize: 26,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                " Utpal",
                style: TextStyle(
                  color: kPrimaryDarkColor,
                  fontSize: 26,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 6,
          ),
          Text(
            "Welcome to Pet Walk App",
            style: TextStyle(
              color: kPrimaryTextColor,
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }
}

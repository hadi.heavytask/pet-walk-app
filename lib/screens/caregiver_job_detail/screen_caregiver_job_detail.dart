import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pet_walk/config/constants.dart';
import 'package:pet_walk/cubit/cubit/selected_pets_cubit.dart';
import 'package:pet_walk/widgets/button.dart';
import 'package:pet_walk/widgets/text_heading.dart';

import 'components/pet_list.dart';

class CaregiverJobDetailScreen extends StatelessWidget {
  static const routeName = '/caregiver-job-detail';
  final _petList = [
    {"name": "Spike"},
    {"name": "Bingo"},
  ];

  @override
  Widget build(BuildContext context) {
    print("requested screen is built...");
    //TODO: DELETE COMMENTS
    // var receivedArguments = ModalRoute.of(context).settings.arguments as Map;
    // print(receivedArguments);
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 8,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: TextHeading(
                text: "Job Detail",
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ListTile(
                      title: Text("Type of Service"),
                      subtitle: Text('Pet Serivce 1, 30 minutes'),
                      trailing: Text('\$30'),
                    ),
                    ListTile(
                      title: Text("Home Address"),
                      subtitle: Text("4532 Formula Lane, Dallas, TX"),
                    ),
                    ListTile(
                      title: Text("Time & Date"),
                      subtitle: Text('January 10, 2021'),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: BookingCostColumn(
                petList: _petList,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BookingCostColumn extends StatelessWidget {
  const BookingCostColumn({
    Key key,
    @required List<Map<String, String>> petList,
  })  : _petList = petList,
        super(key: key);

  final List<Map<String, String>> _petList;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PetListColumn(petList: _petList),
        SizedBox(height: 4),
        Column(
          children: [
            Container(
              padding: const EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  color: kPrimaryExtraLightColor,
                  borderRadius: BorderRadius.circular(10)),
              child: ListTile(
                leading: Text(
                  "Total",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                trailing: Text(
                  '\$60',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(height: 8),
            Container(
              width: double.infinity,
              child: Button(onPressed: () {}, label: "Apply Now"),
            ),
            SizedBox(height: 24),
          ],
        ),
      ],
    );
  }
}

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'selected_pets_state.dart';

class SelectedPetsCubit extends Cubit<SelectedPetsState> {
  SelectedPetsCubit() : super(SelectedPetsInitial([]));

  addToSelectedPets(pet) {
    state.petList.add(pet);
    emit(SelectedPets(state.petList));
  }

  removeFromSelectedPets(pet) {
    state.petList.remove(pet);
    emit(SelectedPets(state.petList));
  }
}

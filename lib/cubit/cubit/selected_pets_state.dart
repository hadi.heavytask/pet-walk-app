part of 'selected_pets_cubit.dart';

@immutable
abstract class SelectedPetsState {
  final List petList;

  SelectedPetsState(this.petList);
}

class SelectedPetsInitial extends SelectedPetsState {
  SelectedPetsInitial(List petList) : super(petList);
}

class SelectedPets extends SelectedPetsState {
  SelectedPets(List petList) : super(petList);
}

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pet_walk/models/pet_model.dart';

part 'pet_list_state.dart';

class PetListCubit extends Cubit<PetListState> {
  PetListCubit() : super(PetListInitial([]));

  addToPetList(pet) {
    state.petList.add(pet);
    emit(PetList(state.petList));
  }
}

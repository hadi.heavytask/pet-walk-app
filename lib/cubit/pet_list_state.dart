part of 'pet_list_cubit.dart';

@immutable
abstract class PetListState {
  final List<Pet> petList;

  PetListState(this.petList);
}

class PetListInitial extends PetListState {
  PetListInitial(List<Pet> petList) : super(petList);
}

class PetList extends PetListState {
  PetList(List<Pet> petList) : super(petList);
}

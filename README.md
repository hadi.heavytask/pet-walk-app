# Pet Walk (Flutter App)

Pet walker is a platform to help and connect pet lovers and service providers. The model has two category users, pet owners, and walkers. A pet owner can leverage this cutting-edge technology-based solution and keep their pets healthy.

## The Solution

Our solution is a win-win for both pet owners and pet walkers. We mitigate the challenge of dog owners by ensuring that their pets get the attention they deserve. A professional can take care of your pets better than anyone. Our solution makes hiring professional pet walkers as simple as a click. This app ensures that the pets are in good hands and the pet walkers have a source of earning while they’re exercising outdoors.

# App Preview

<p float="left">
  <img src="/readme-images/app-preview-1.png" width="160" />
  <img src="/readme-images/app-preview-2.png" width="160" /> 
  <img src="/readme-images/app-preview-3.png" width="160" />
  <img src="/readme-images/app-preview-4.png" width="160" />
</p>

<p float="left">
  <img src="/readme-images/app-preview-5.png" width="160" />
  <img src="/readme-images/app-preview-6.png" width="160" /> 
  <img src="/readme-images/app-preview-7.png" width="160" />
  
</p>

# Getting Started

1. [Download](https://flutter.dev/docs/get-started/install) Flutter SDK.
2. [Download](https://developer.android.com/studio/) Android Studio and install flutter plugin.
3. [OPTIONAL] [Download](https://code.visualstudio.com/Download) VS Code and install flutter plugin in it. (If you want to code in VS Code only, but you must have Android Studio installed on your system.)
4. Clone this repository, Terminal: `git clone https://gitlab.com/heavytask/pet-walk-app.git`
5. Run the app, Debug > Run without debugging in VS Code, also you can run the app with terminal by `flutter run`

Find more information to get started check the official [documentation](https://flutter.dev/docs/get-started/editor?tab=androidstudio).

## How to Contribute

You can submit feedback and report bugs as Github issues. Please be sure to include your operating system, device, version number, and steps to reproduce reported bugs.

### Request or submit a feature

Would you like to request a feature? Please get in touch with us on [LinkedIn](https://www.linkedin.com/company/heavytask/mycompany/)

If you’d like to contribute code with a Pull Request, please make sure to follow code submission guidelines. Create your own branch and then pull a request.

### Spread the word

To learn more about us, join the conversation:

- [LinkedIn](https://www.linkedin.com/company/heavytask/mycompany/)
- [Facebook](https://www.facebook.com/HeavyTaskIT/)
- [Instagram](https://www.instagram.com/heavytask/?hl=en)

## Contributor

<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<a href="https://www.linkedin.com/company/heavytask/mycompany/"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/5459215/avatar.png" width="100px;" alt=""/><br /><sub><b>HeavyTask Software Team</b></sub></a>


<!-- markdownlint-enable -->
<!-- prettier-ignore-end -->
